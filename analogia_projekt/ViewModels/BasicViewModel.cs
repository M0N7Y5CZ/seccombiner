﻿using Prism.Commands;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace analogia_projekt
{


    public class SCRCModel
    {
        public string RCIP { get; set; }
        public string SCSetIP { get; set; }
        public string SCSetIPMask { get; set; }
        public string SCSetGateway { get; set; }
        public string SCIPSCRC { get; set; }
    }


    public class BasicViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public BasicViewModel()
        {
            
        }

    }
}