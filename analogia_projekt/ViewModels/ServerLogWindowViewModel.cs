﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;

namespace analogia_projekt.ViewModels
{
    class ServerLogWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string ServerResponse { get; set; }

        private readonly DelegateCommand saveResponseCommand;
        public ICommand SaveResponseCommand => saveResponseCommand;

        public ServerLogWindowViewModel()
        {
            saveResponseCommand = new DelegateCommand(SaveResponse);
        }

        private void SaveResponse()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Server Response"; // Default file name
            dlg.DefaultExt = ".text"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                try
                {
                    File.WriteAllText(dlg.FileName, ServerResponse);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                MessageBox.Show("Uloženo!");
            }

        }
    }
}
