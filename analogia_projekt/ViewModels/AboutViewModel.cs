﻿using analogia_projekt.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace analogia_projekt.ViewModels
{

    class AboutViewModel
    {
        public string AppVersion { get; } = "1.0.6";
    }
}
