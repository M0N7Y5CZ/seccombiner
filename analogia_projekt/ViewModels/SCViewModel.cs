﻿using analogia_projekt.Properties;
using analogia_projekt.Helpers;
using Prism.Commands;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using analogia_projekt.Views;
using Meta.Vlc.Wpf;
using System.Text.RegularExpressions;
using Renci.SshNet;
using System.Threading;
using System.Windows.Media;
using Meta.Vlc;

namespace analogia_projekt.ViewModels
{

    public class SCViewModel : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // Deklarace Vícevláknových Image elemntů
        public ThreadSeparatedImage Cam1 { get; set; }
        public ThreadSeparatedImage Mix { get; set; }
        public ThreadSeparatedImage Cam2 { get; set; }

        // Deklarace VLCPlayerů
        private VlcPlayer Cam1Player, Cam2Player, MixPlayer = null;

        string[] VlcOptions = {
            "--no-video-title", $"--network-caching={Settings.Default.VlcSetBuffer}", "--clock-synchro=0", "--clock-jitter=20", "--cr-average=100", "--rtsp-timeout=3"};

        // Cesta ke knihovnám VlcPlayeru + nastavení ...
        string LibVlcPath = @"LibVlc";

        ConnectionInfo conInfo;

        ServerStatus serverStatus;

        private string _sCSetIPMask;

        public string SCIP { get; set; }
        public string SCSetIP { get; set; }

        public string SCSetIPMask
        {
            get => _sCSetIPMask;
            set
            {
                if (value.Length <= 2)
                {
                    if (string.IsNullOrEmpty(value))
                        _sCSetIPMask = string.Empty;

                    sbyte tmp;
                    if (sbyte.TryParse(value, out tmp))
                        if (tmp <= 32 && tmp >= 1)
                            _sCSetIPMask = value;
                }
            }
        }
        public string SCSetGateway { get; set; }
        public string SCIPSCRC { get; set; }

        public bool SCConnected { get; set; } = false;
        public bool SCIPEmpty { get => !string.IsNullOrWhiteSpace(SCIP); }
        public bool SCIsReqInfoEmpty => !string.IsNullOrWhiteSpace(SCSetIP) && !string.IsNullOrWhiteSpace(SCSetIPMask)
                && !string.IsNullOrWhiteSpace(SCSetGateway) && !string.IsNullOrWhiteSpace(SCIPSCRC) && SCConnected;

        public bool PlayerVisibility { get; set; } = false;

        #region Delegace comandů
        private readonly DelegateCommand scsshConnectCommand;
        public ICommand SCSSHConnectCommand => scsshConnectCommand;

        private readonly DelegateCommand scsshDisconnectCommand;
        public ICommand SCSSHDisconnectCommand => scsshDisconnectCommand;

        private readonly DelegateCommand runSCTerminalCommand;
        public ICommand RunSCTerminalCommand => runSCTerminalCommand;

        private readonly DelegateCommand runSCVNCClientCommand;
        public ICommand RunVNCClientCommand => runSCVNCClientCommand;

        private readonly DelegateCommand setupSCCommand;
        public ICommand SetupSCRSCommand => setupSCCommand;

        private readonly DelegateCommand getscKernelLogCommand;
        public ICommand GetSCKernelLogCommand => getscKernelLogCommand;

        private readonly DelegateCommand getSSHLogCommand;
        public ICommand GetSSHLogCommand => getSSHLogCommand;

        private readonly DelegateCommand camerasTestCommand;
        public ICommand CamerasTestCommand => camerasTestCommand;

        private readonly DelegateCommand showUSBInfoCommand;
        public ICommand ShowUSBInfoCommand => showUSBInfoCommand;

        private readonly DelegateCommand softRestartCommand;
        public ICommand SoftRestartCommand => softRestartCommand;

        private readonly DelegateCommand hardRestartCommand;
        public ICommand HardRestartCommand => hardRestartCommand;

        private readonly DelegateCommand openSettingsWindowCommand;
        public ICommand OpenSettingsWindowCommand => openSettingsWindowCommand;
        #endregion

        public SCViewModel()
        {

            Application.Current.MainWindow.Closing += MainWindow_Closing;
            // Inicializace Vícevláknových Image elementů
            Cam1 = new ThreadSeparatedImage();
            Mix = new ThreadSeparatedImage();
            Cam2 = new ThreadSeparatedImage();

            // Incializace Vlcplayerů
            Cam1Player = new VlcPlayer(true);
            Cam2Player = new VlcPlayer(true);
            MixPlayer = new VlcPlayer(true);

            #region VlcPlayer bude přijatá data posílat do příslušných vláken
            Cam1Player.Initialize(LibVlcPath, VlcOptions);
            Cam1Player.VideoSourceChanged += Cam1Player_VideoSourceChanged;

            Cam2Player.Initialize(LibVlcPath, VlcOptions);
            Cam2Player.VideoSourceChanged += Cam2Player_VideoSourceChanged;

            MixPlayer.Initialize(LibVlcPath, VlcOptions);
            MixPlayer.VideoSourceChanged += MixPlayer_VideoSourceChanged;
            #endregion

            //Inicializace pingovací třídy
            serverStatus = new ServerStatus();

            // Nastavení eventú pro pingovací vlákno
            serverStatus.workerServerStatus.ProgressChanged += WorkerServerStatus_ProgressChanged;
            serverStatus.workerServerStatus.RunWorkerCompleted += WorkerServerStatus_RunWorkerCompleted;

            #region incializace komandů
            // Inicializace Komandů pro View
            scsshConnectCommand = new DelegateCommand(SCSSHConnect).ObservesCanExecute(() => SCIPEmpty);
            runSCTerminalCommand = new DelegateCommand(() => RunProcess("putty.exe", $"-ssh {SCIP}")).ObservesCanExecute(() => SCConnected);
            runSCVNCClientCommand = new DelegateCommand(() => RunProcess("vncviewer.exe", SCIP)).ObservesCanExecute(() => SCConnected);
            setupSCCommand = new DelegateCommand(setupSC).ObservesCanExecute(() => SCIsReqInfoEmpty);
            getscKernelLogCommand = new DelegateCommand(() => GetServerResponse("dmesg -L --color=never")).ObservesCanExecute(() => SCConnected);
            camerasTestCommand = new DelegateCommand(CamerasTest).ObservesCanExecute(() => SCConnected);
            showUSBInfoCommand = new DelegateCommand(() => GetServerResponse("lsusb")).ObservesCanExecute(() => SCConnected);
            softRestartCommand = new DelegateCommand(() => GetServerResponse($"bash reboot.sh")).ObservesCanExecute(() => SCConnected);
            hardRestartCommand = new DelegateCommand(() => GetServerResponse("./service.driver.pwr.off", false)).ObservesCanExecute(() => SCConnected);
            scsshDisconnectCommand = new DelegateCommand(SSHDisconnect);
            getSSHLogCommand = new DelegateCommand(() => GetServerResponse("cat /var/log/auth.log")).ObservesCanExecute(() => SCConnected);
            openSettingsWindowCommand = new DelegateCommand(OpenSettingsWindow);
            #endregion
        }

        private void MixPlayer_VideoSourceChanged(object sender, VideoSourceChangedEventArgs e)
        {
            Mix.Dispatcher.BeginInvoke(new Action(() =>
            {
                Mix.Source = e.NewVideoSource;
            }));
        }

        private void Cam2Player_VideoSourceChanged(object sender, VideoSourceChangedEventArgs e)
        {
            Cam2.Dispatcher.BeginInvoke(new Action(() =>
            {
                Cam2.Source = e.NewVideoSource;
            }));
        }

        private void Cam1Player_VideoSourceChanged(object sender, VideoSourceChangedEventArgs e)
        {
            Cam1.Dispatcher.BeginInvoke(new Action(() =>
            {
                Cam1.Source = e.NewVideoSource;
            }));
        }

        /// <summary>
        /// Otevře okno s nastavením
        /// </summary>
        private void OpenSettingsWindow()
        {
            var settingsWindow = new UserSettingsWindow();
            settingsWindow.ShowDialog();
        }

        /// <summary>
        /// Před zavřením okna uvolní zdroje aplikace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            this.Dispose();
        }

        #region Kamery
        private void Mix_VlcMediaPlayer_EncounteredError(object sender, EventArgs e)
        {
            Console.WriteLine("Mix Chyba: " + e);
        }

        private void Cam2_VlcMediaPlayer_EncounteredError(object sender, EventArgs e)
        {
            Console.WriteLine("Cam2 Chyba: " + e);
        }

        private void Cam1_VlcMediaPlayer_EncounteredError(object sender, EventArgs e)
        {
            Console.WriteLine("Cam1 Chyba: " + e);
        }

        private void Cam1_MediaFailed(object sender, ExceptionRoutedEventArgs e) => Console.WriteLine("CAM1: " + e.ErrorException.Message);
        #endregion

        /// <summary>
        /// Zastaví pingovací vlákno
        /// </summary>
        private void SSHDisconnect()
        {
            serverStatus.workerServerStatus.CancelAsync();
            SCConnected = false;
        }

        /// <summary>
        /// Získá pomocí definovaného příkazu odpověď serveru a vyvolá dialog
        /// </summary>
        /// <param name="cmd">Příkaz</param>
        /// <param name="showDialog">Zobrazit dialog?</param>
        private async void GetServerResponse(string cmd, bool showDialog = true)
        {
            var dialog = new ServerLogWindow();
            dialog.DataContext = new ServerLogWindowViewModel()
            {
                ServerResponse = await Task.Run<string>(() =>
                {
                    try
                    {
                        string result;
                        using (SshClient client = new SshClient(conInfo))
                        {
                            client.Connect();
                            var _cmd = client.CreateCommand(cmd);
                            var _async = _cmd.BeginExecute();
                            while (!_async.IsCompleted)
                            {
                                Thread.Sleep(1000);
                            }
                            result = _cmd.EndExecute(_async);
                            client.Disconnect();
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        return "Chyba při zpracování požadavku: " + ex;
                    }

                })
            };
            if (showDialog)
                dialog.ShowDialog();
        }

        /// <summary>
        /// Pošle asynchroní příkaz na server a získá odpověď
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private string SendCommandAsync(string cmd)
        {
            try
            {
                string result;
                using (SshClient client = new SshClient(conInfo))
                {
                    client.Connect();
                    var _cmd = client.CreateCommand(cmd);
                    var _async = _cmd.BeginExecute();
                    while (!_async.IsCompleted)
                    {
                        Thread.Sleep(1000);
                    }
                    result = _cmd.EndExecute(_async);
                    client.Disconnect();
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return "Chyba při zpracování požadavku: " + ex;
            }
        }

        /// <summary>
        /// Připojí VLC plugin na GStreamer server 
        /// </summary>
        private void CamerasTest()
        {
            try
            {
                //Cam1Player?.Dispose();
                //Cam2Player?.Dispose();
                //MixPlayer?.Dispose();

                if (Cam1Player.VlcMediaPlayer.IsPlaying && Cam2Player.VlcMediaPlayer.IsPlaying && MixPlayer.VlcMediaPlayer.IsPlaying)
                {
                    Cam1Player.Stop();
                    Cam2Player.Stop();
                    MixPlayer.Stop();

                    Cam1Player.Dispose();
                    Cam2Player.Dispose();
                    MixPlayer.Dispose();

                    Cam1Player = new VlcPlayer(true);
                    Cam2Player = new VlcPlayer(true);
                    MixPlayer = new VlcPlayer(true);

                    #region VlcPlayer bude přijatá data posílat do příslušných vláken
                    Cam1Player.Initialize(LibVlcPath, VlcOptions);
                    Cam1Player.VideoSourceChanged += Cam1Player_VideoSourceChanged;

                    Cam2Player.Initialize(LibVlcPath, VlcOptions);
                    Cam2Player.VideoSourceChanged += Cam2Player_VideoSourceChanged;

                    MixPlayer.Initialize(LibVlcPath, VlcOptions);
                    MixPlayer.VideoSourceChanged += MixPlayer_VideoSourceChanged;

                    Cam1Player.VlcMediaPlayer.Playing += VlcMediaPlayer_Playing;
                    Cam1Player.VlcMediaPlayer.Stoped += VlcMediaPlayer_Stoped;
                    Cam1Player.VlcMediaPlayer.EncounteredError += VlcMediaPlayer_EncounteredError;
                    #endregion
                }

                if(Cam1Player.VlcMediaPlayer.State != Meta.Vlc.Interop.Media.MediaState.Opening &&
                    Cam2Player.VlcMediaPlayer.State != Meta.Vlc.Interop.Media.MediaState.Opening &&
                    MixPlayer.VlcMediaPlayer.State != Meta.Vlc.Interop.Media.MediaState.Opening)
                {
                    Cam1Player.Stop();
                    Cam2Player.Stop();
                    MixPlayer.Stop();

                    Cam1Player.LoadMediaWithOptions(new Uri($"rtsp://{SCIP}:8554/1cam"));
                    Cam2Player.LoadMediaWithOptions(new Uri($"rtsp://{SCIP}:8554/2cam"));
                    MixPlayer.LoadMediaWithOptions(new Uri($"rtsp://{SCIP}:8554/mix"));

                    Cam1Player.Play();
                    Cam2Player.Play();
                    MixPlayer.Play();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void VlcMediaPlayer_EncounteredError(object sender, EventArgs e)
        {
            Cam1Player.Stop();
            Cam2Player.Stop();
            MixPlayer.Stop();
        }

        private void VlcMediaPlayer_Stoped(object sender, Meta.Vlc.ObjectEventArgs<Meta.Vlc.Interop.Media.MediaState> e)
        {
            PlayerVisibility = false;
        }

        private void VlcMediaPlayer_Playing(object sender, Meta.Vlc.ObjectEventArgs<Meta.Vlc.Interop.Media.MediaState> e)
        {
            PlayerVisibility = true;
        }

        /// <summary>
        /// Nastaví požadované hodnoty na serveru
        /// </summary>
        private void setupSC()
        {
            if (!IsIPValid(SCSetIP)) { MessageBox.Show("IP adresa nebo Maska není validní!"); }
            else if (!IsIPValid(SCSetGateway)) { MessageBox.Show("IP adresa brány není validní!"); }
            else if (!IsIPValid(SCIPSCRC)) { MessageBox.Show("IP adresa SCRC není validní!"); }
            else
            {
                string setupCmd = $"echo \"sudo ip addr add {SCSetIP}/{SCSetIPMask} brd + dev eth0\" > ipconfig.sh" + " && " +
                $"echo \"sudo ip route add default via {SCSetGateway}\" >> ipconfig.sh" + " && " +
                $"echo " + "\"# run_client.sh\n" + "while true\n" + "do\n" + "  echo tcpclient start\n" + $"  ./service.driver.TCPclient 9013 {SCIPSCRC}\n" + "  echo 'tcpclient stopped itself (called exit)'\n" + "  sleep 10\n" + "done\" > Run_client.sh";
                GetServerResponse(setupCmd, false);
                MessageBox.Show("Příkaz odeslán!");
            }
        }

        /// <summary>
        /// Přečte konfiguraci serveru 
        /// </summary>
        private void ReadServerSetupAsync()
        {
            string ipconfig_sh = SendCommandAsync("cat ipconfig.sh");
            string run_client_sh = SendCommandAsync("cat Run_client.sh");

            string re1 = ".*?"; // Non-greedy match on filler
            string re2 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";   // IPv4 IP Address 1
            string re3 = ".*?"; // Non-greedy match on filler
            string re4 = "((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])";  // Day 1
            string re5 = ".*?"; // Non-greedy match on filler
            string re6 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";   // IPv4 IP Address 2

            Regex r = new Regex(re1 + re2 + re3 + re4 + re5 + re6, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(ipconfig_sh);
            Console.WriteLine(ipconfig_sh);
            if (m.Success)
            {
                SCSetIP = m.Groups[1].ToString();
                SCSetIPMask = m.Groups[2].ToString();
                SCSetGateway = m.Groups[3].ToString();
            }

            string re11 = ".*?"; // Non-greedy match on filler
            string re21 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";   // IPv4 IP Address 1

            Regex r1 = new Regex(re11 + re21, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m1 = r1.Match(run_client_sh);
            if (m1.Success)
            {
                SCIPSCRC = m1.Groups[1].ToString();
            }
        }

        /// <summary>
        /// Metoda pro validaci IP Adres
        /// </summary>
        /// <param name="ip">IP adresa, kterou chceme validovat</param>
        /// <returns></returns>
        /// 
        private bool IsIPValid(string ip)
        {
            IPAddress address;
            return IPAddress.TryParse(ip, out address);

        }

        /// <summary>
        /// Zkouší se připojit a odešle testovací command na server
        /// </summary>
        private async void SCSSHConnect()
        {
            await Task.Run(() =>
            {
                try
                {
                    if (!IsIPValid(SCIP))
                    {
                        MessageBox.Show("IP Adresa hostitele není validní!");
                        return;
                    }
                    else
                    {
                        // Setup session options
                        conInfo = new ConnectionInfo(SCIP, Settings.Default.DefaultSCUser, new PasswordAuthenticationMethod(Settings.Default.DefaultSCUser, Settings.Default.DefaultSCPassword));
                        conInfo.RetryAttempts = 3;
                        conInfo.Timeout = TimeSpan.FromSeconds(3);

                        Console.WriteLine($"Připojuji se na: {SCIP} \n Uživatel: {Settings.Default.DefaultSCUser} \n Heslo: {Settings.Default.DefaultSCPassword}");
                        string result;
                        using (SshClient client = new SshClient(conInfo))
                        {
                            client.Connect();
                            var _cmd = client.CreateCommand("uptime");
                            var _async = _cmd.BeginExecute();
                            while (!_async.IsCompleted)
                            {
                                Thread.Sleep(1000);
                            }
                            result = _cmd.EndExecute(_async);
                            client.Disconnect();
                        }

                        //workerServerStatus.RunWorkerAsync(SCIP);
                        serverStatus.workerServerStatus.RunWorkerAsync(SCIP);

                        Console.WriteLine(result);
                        SCConnected = true;
                    }
                    ReadServerSetupAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(ex.Message);
                }
            });

            #region test
            //try
            //{
            //    scClient = new SshHelper(this.SCIP, "root", "Kamilka3");
            //    scClient.ConnTest();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Chyba: " + ex.Message);
            //}
            #endregion
        }

        /// <summary>
        /// Spustí proces s danými parametry
        /// </summary>
        /// <param name="executable">Cesta k souboru</param>
        /// <param name="args">Argumenty</param>
        private void RunProcess(string executable, string args)
        {
            try
            {
                var proc = Process.Start(executable, args);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Chyba: {ex.Message}");
            }
        }

        #region Pingovací vlákno a jeho raisovací metody
        private void WorkerServerStatus_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var hostIsPingable = (bool)e.UserState;
            if (!hostIsPingable)
            {
                ((BackgroundWorker)sender).CancelAsync();
                SCConnected = hostIsPingable;
            }
        }

        private void WorkerServerStatus_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //session.Close();

            if (e.Cancelled)
            {
                SCConnected = false;
                Console.WriteLine("Server Status Worker: Přerušeno uživatelem...");
            }

            SCConnected = false;

        }
        #endregion

        public void Dispose()
        {
            if (Cam1Player != null)
            {
                Cam1Player.Stop();
                Cam1Player.Dispose();
            }

            if (Cam2Player != null)
            {
                Cam2Player.Stop();
                Cam2Player.Dispose();
            }

            if (MixPlayer != null)
            {
                MixPlayer.Stop();
                MixPlayer.Dispose();
            }

            serverStatus?.Dispose();
        }

    }
}
