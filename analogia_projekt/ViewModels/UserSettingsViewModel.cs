﻿using analogia_projekt.Properties;
using analogia_projekt.Views;
using Prism.Commands;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace analogia_projekt.ViewModels
{
    class UserSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string SCUser { get; set; } = Settings.Default.DefaultSCUser;
        public string SCPass { get; set; } = Settings.Default.DefaultSCPassword;

        public string SCRCUser { get; set; } = Settings.Default.DefaultSCRCUser;
        public string SCRCPass { get; set; } = Settings.Default.DefaultSCRCPassword;

        public string VlcBuffer {
            get => _vlcBuffer;
            set {

                if (string.IsNullOrEmpty(value))
                    _vlcBuffer = 0.ToString();

                int temp;
                if(int.TryParse(value, out temp))
                {
                    _vlcBuffer = value;
                }

            }
        }

        public bool CanSave
        {
            get => _canSave = (!string.IsNullOrWhiteSpace(SCUser) && !string.IsNullOrWhiteSpace(SCPass)) &&
                    (!string.IsNullOrWhiteSpace(SCRCUser) && !string.IsNullOrWhiteSpace(SCRCPass));
        }
        private bool _canSave;
        private string _vlcBuffer;
        private readonly DelegateCommand saveSettingsCommand;
        public ICommand SaveSettingsCommand => saveSettingsCommand;

        private readonly DelegateCommand openAboutWindowCommand;
        public ICommand OpenAboutWindowCommand => openAboutWindowCommand;

        public UserSettingsViewModel()
        {
            openAboutWindowCommand = new DelegateCommand(OpenAboutWindow);
            saveSettingsCommand = new DelegateCommand(SaveSettings).ObservesCanExecute(() => CanSave);
            VlcBuffer = Settings.Default.VlcSetBuffer;
        }

        private void OpenAboutWindow()
        {
            var aboutwindow = new AboutWindow();
            aboutwindow.ShowDialog();
        }

        private void SaveSettings()
        {
            Settings.Default.DefaultSCUser = SCUser;
            Settings.Default.DefaultSCPassword = SCPass;
            Settings.Default.DefaultSCRCUser = SCRCUser;
            Settings.Default.DefaultSCRCPassword = SCRCPass;
            Settings.Default.VlcSetBuffer = VlcBuffer;
            try
            {
                Settings.Default.Save();
                MessageBox.Show("Uloženo!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

        }
    }


}
