﻿using analogia_projekt.Properties;
using analogia_projekt.Helpers;
using Prism.Commands;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using analogia_projekt.Views;
using System.Text.RegularExpressions;
using Renci.SshNet;
using System.Threading;

namespace analogia_projekt.ViewModels
{

    public class SCRCViewModel : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        ConnectionInfo conInfo;

        ServerStatus serverStatus;

        private string _sCrcSetIPMask;

        public string SCRCIP { get; set; }
        public string SCRCSetIP { get; set; }

        public string SCRCSetIPMask
        {
            get => _sCrcSetIPMask;
            set
            {
                if (value.Length <= 2)
                {
                    if (string.IsNullOrEmpty(value))
                        _sCrcSetIPMask = string.Empty;

                    sbyte tmp;
                    if (sbyte.TryParse(value, out tmp))
                        if (tmp <= 32 && tmp >= 1)
                            _sCrcSetIPMask = value;
                }
            }
        }
        public string SCRCSetGateway { get; set; }
        public string SCRCIPSC { get; set; }

        public bool SCRCConnected { get; set; } = false;
        public bool SCRCIPEmpty { get => !string.IsNullOrWhiteSpace(SCRCIP); }
        public bool SCRCIsReqInfoEmpty => !string.IsNullOrWhiteSpace(SCRCSetIP) && !string.IsNullOrWhiteSpace(SCRCSetIPMask)
                && !string.IsNullOrWhiteSpace(SCRCSetGateway) && !string.IsNullOrWhiteSpace(SCRCIPSC) && SCRCConnected;

        #region Delegace comandů
        private readonly DelegateCommand scrcsshConnectCommand;
        public ICommand SCRCSSHConnectCommand => scrcsshConnectCommand;

        private readonly DelegateCommand scrcsshDisconnectCommand;
        public ICommand SCRCSSHDisconnectCommand => scrcsshDisconnectCommand;

        private readonly DelegateCommand runSCrcTerminalCommand;
        public ICommand RunSCRCTerminalCommand => runSCrcTerminalCommand;

        private readonly DelegateCommand runSCrcVNCClientCommand;
        public ICommand RunVNCClientCommand => runSCrcVNCClientCommand;

        private readonly DelegateCommand setupSCrcCommand;
        public ICommand SetupSCCommand => setupSCrcCommand;

        private readonly DelegateCommand getscrcKernelLogCommand;
        public ICommand GetSCRCKernelLogCommand => getscrcKernelLogCommand;

        private readonly DelegateCommand getSSHLogCommand;
        public ICommand GetSSHLogCommand => getSSHLogCommand;

        private readonly DelegateCommand showUSBInfoCommand;
        public ICommand ShowUSBInfoCommand => showUSBInfoCommand;

        private readonly DelegateCommand softRestartCommand;
        public ICommand SoftRestartCommand => softRestartCommand;
        #endregion

        public SCRCViewModel()
        {
            Application.Current.MainWindow.Closing += MainWindow_Closing;

            //Inicializace pingovací třídy
            serverStatus = new ServerStatus();

            // Nastavení eventú pro pingovací vlákno
            serverStatus.workerServerStatus.ProgressChanged += WorkerServerStatus_ProgressChanged;
            serverStatus.workerServerStatus.RunWorkerCompleted += WorkerServerStatus_RunWorkerCompleted;

            #region incializace komandů
            // Inicializace Komandů pro View
            scrcsshConnectCommand = new DelegateCommand(SCRCSSHConnect).ObservesCanExecute(() => SCRCIPEmpty);
            runSCrcTerminalCommand = new DelegateCommand(() => RunProcess("putty.exe", $"-ssh {SCRCIP}")).ObservesCanExecute(() => SCRCConnected);
            runSCrcVNCClientCommand = new DelegateCommand(() => RunProcess("vncviewer.exe", SCRCIP)).ObservesCanExecute(() => SCRCConnected);
            setupSCrcCommand = new DelegateCommand(setupSC).ObservesCanExecute(() => SCRCIsReqInfoEmpty);
            getscrcKernelLogCommand = new DelegateCommand(() => GetServerResponse("dmesg -L --color=never")).ObservesCanExecute(() => SCRCConnected);
            showUSBInfoCommand = new DelegateCommand(() => GetServerResponse("lsusb")).ObservesCanExecute(() => SCRCConnected);
            softRestartCommand = new DelegateCommand(() => GetServerResponse($"bash reboot.sh", false)).ObservesCanExecute(() => SCRCConnected);
            scrcsshDisconnectCommand = new DelegateCommand(SSHDisconnect);
            getSSHLogCommand = new DelegateCommand(() => GetServerResponse("cat /var/log/auth.log")).ObservesCanExecute(() => SCRCConnected);
            #endregion
        }

        /// <summary>
        /// Zkouší se připojit a odešle testovací command na server
        /// </summary>
        private async void SCRCSSHConnect()
        {
            await Task.Run(() =>
            {
                try
                {
                    if (!IsIPValid(SCRCIP))
                    {
                        MessageBox.Show("IP Adresa hostitele není validní!");
                        return;
                    }
                    else
                    {
                        // Setup session options
                        conInfo = new ConnectionInfo(SCRCIP, Settings.Default.DefaultSCRCUser, new PasswordAuthenticationMethod(Settings.Default.DefaultSCRCUser, Settings.Default.DefaultSCRCPassword));
                        conInfo.RetryAttempts = 3;
                        conInfo.Timeout = TimeSpan.FromSeconds(3);

                        Console.WriteLine($"Připojuji se na: {SCRCIP} \n Uživatel: {Settings.Default.DefaultSCRCUser} \n Heslo: {Settings.Default.DefaultSCRCPassword}");
                        string result;
                        using (SshClient client = new SshClient(conInfo))
                        {
                            client.Connect();
                            var _cmd = client.CreateCommand("uptime");
                            var _async = _cmd.BeginExecute();
                            while (!_async.IsCompleted)
                            {
                                Thread.Sleep(1000);
                            }
                            result = _cmd.EndExecute(_async);
                            client.Disconnect();
                        }

                        //workerServerStatus.RunWorkerAsync(SCIP);
                        serverStatus.workerServerStatus.RunWorkerAsync(SCRCIP);

                        Console.WriteLine(result);
                        SCRCConnected = true;
                    }
                    ReadServerSetupAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(ex.Message);
                }
            });

            #region test
            //try
            //{
            //    scClient = new SshHelper(this.SCIP, "root", "Kamilka3");
            //    scClient.ConnTest();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Chyba: " + ex.Message);
            //}
            #endregion
        }

        /// <summary>
        /// Při ukončování aplikace uvolní zdroje tříd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Closing(object sender, CancelEventArgs e) => this.Dispose();

        /// <summary>
        /// Odpojí se od serveru
        /// </summary>
        private void SSHDisconnect()
        {
            serverStatus.workerServerStatus.CancelAsync();
            SCRCConnected = false;
        }

        /// <summary>
        /// Pošle příkaz a odpověď zobrazí v dialogu
        /// </summary>
        /// <param name="cmd">Příkaz</param>
        /// <param name="showDialog">Zobrazit dialog?</param>
        private async void GetServerResponse(string cmd, bool showDialog = true)
        {
            var dialog = new ServerLogWindow();
            dialog.DataContext = new ServerLogWindowViewModel()
            {
                ServerResponse = await Task.Run<string>(() =>
                {
                    try
                    {
                        string result;
                        using (SshClient client = new SshClient(conInfo))
                        {
                            client.Connect();
                            var _cmd = client.CreateCommand(cmd);
                            var _async = _cmd.BeginExecute();
                            while (!_async.IsCompleted)
                            {
                                Thread.Sleep(1000);
                            }
                            result = _cmd.EndExecute(_async);
                            client.Disconnect();
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        return "Chyba při zpracování požadavku: " + ex;
                    }

                })
            };
            if (showDialog)
                dialog.ShowDialog();
        }

        /// <summary>
        /// Pošle asynchronní příkaz na server a získá odpoveď 
        /// </summary>
        /// <param name="cmd">Příkaz</param>
        /// <returns>Vrací odpověď na příkaz</returns>
        private string SendCommandAsync(string cmd)
        {
            try
            {
                string result;
                using (SshClient client = new SshClient(conInfo))
                {
                    client.Connect();
                    var _cmd = client.CreateCommand(cmd);
                    var _async = _cmd.BeginExecute();
                    while (!_async.IsCompleted)
                    {
                        Thread.Sleep(1000);
                    }
                    result = _cmd.EndExecute(_async);
                    client.Disconnect();
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return "Chyba při zpracování požadavku: " + ex;
            }
        }

        /// <summary>
        /// Nastaví serveru zadané hodnoty
        /// </summary>
        private void setupSC()
        {
            if (!IsIPValid(SCRCSetIP)) { MessageBox.Show("IP adresa nebo Maska není validní!"); }
            else if (!IsIPValid(SCRCSetGateway)) { MessageBox.Show("IP adresa brány není validní!"); }
            else if (!IsIPValid(SCRCIPSC)) { MessageBox.Show("IP adresa SCRC není validní!"); }
            else
            {
                string setupCmd = $"echo \"sudo ip addr add {SCRCSetIP}/{SCRCSetIPMask} brd + dev eth0\" > ipconfig.sh" + " && " +
                $"echo \"sudo ip route add default via {SCRCSetGateway}\" >> ipconfig.sh" + " && " +
                $"echo " + "\"# run_client.sh\n" + "while true\n" + "do\n" + "  echo tcpclient start\n" + $"  ./service.driver.TCPclient 9013 {SCRCIPSC}\n" + "  echo 'tcpclient stopped itself (called exit)'\n" + "  sleep 10\n" + "done\" > Run_client.sh";
                GetServerResponse(setupCmd, false);
                MessageBox.Show("Příkaz odeslán!");
            }
        }

        /// <summary>
        /// Načte konfiguraci serveru
        /// </summary>
        private void ReadServerSetupAsync()
        {
            string ipconfig_sh = SendCommandAsync("cat ipconfig.sh");
            string run_client_sh = SendCommandAsync("cat Run_client.sh");

            string re1 = ".*?"; // Non-greedy match on filler
            string re2 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";   // IPv4 IP Address 1
            string re3 = ".*?"; // Non-greedy match on filler
            string re4 = "((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])";  // Day 1
            string re5 = ".*?"; // Non-greedy match on filler
            string re6 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";   // IPv4 IP Address 2

            Regex r = new Regex(re1 + re2 + re3 + re4 + re5 + re6, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(ipconfig_sh);
            Console.WriteLine(ipconfig_sh);
            if (m.Success)
            {
                SCRCSetIP = m.Groups[1].ToString();
                SCRCSetIPMask = m.Groups[2].ToString();
                SCRCSetGateway = m.Groups[3].ToString();
            }

            string re11 = ".*?"; // Non-greedy match on filler
            string re21 = "((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])";   // IPv4 IP Address 1

            Regex r1 = new Regex(re11 + re21, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m1 = r1.Match(run_client_sh);
            if (m1.Success)
            {
                SCRCIPSC = m1.Groups[1].ToString();
            }
        }

        /// <summary>
        /// Metoda pro validaci IP Adres
        /// </summary>
        /// <param name="ip">IP adresa, kterou chceme validovat</param>
        /// <returns></returns>
        /// 
        private bool IsIPValid(string ip)
        {
            IPAddress address;
            return IPAddress.TryParse(ip, out address);

        }


        /// <summary>
        /// Spustí proces s danými parametry
        /// </summary>
        /// <param name="executable">Cesta k souboru</param>
        /// <param name="args">Argumenty</param>
        private void RunProcess(string executable, string args)
        {
            try
            {
                var proc = Process.Start(executable, args);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Chyba: {ex.Message}");
            }
        }

        #region Pingovací vlákno a jeho raisovací metody
        private void WorkerServerStatus_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var hostIsPingable = (bool)e.UserState;
            if (!hostIsPingable)
            {
                ((BackgroundWorker)sender).CancelAsync();
                SCRCConnected = hostIsPingable;
            }
        }

        private void WorkerServerStatus_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                SCRCConnected = false;
                Console.WriteLine("Server Status Worker: Přerušeno uživatelem...");
            }

            SCRCConnected = false;
        }
        #endregion

        public void Dispose()
        {
           // serverStatus.workerServerStatus.
            serverStatus.Dispose();
        }
    }
}
