﻿using System;
using System.Windows;
using System.IO;
using System.Windows.Input;
using System.Windows.Controls;
using System.Diagnostics;
using System.Text.RegularExpressions;
using analogia_projekt.ViewModels;

namespace analogia_projekt
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        string vlcLibDirectory;
        public MainWindow()
        {

            InitializeComponent();
            ConsoleManager.Show();

            vlcLibDirectory = (Path.Combine(Environment.CurrentDirectory, "LibVlc"));
            
        }

        #region Funkce Titlebaru 
        /// <summary>
        /// Přesouvání okna pomocí uchopení horní lišty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void titlebar_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Pokud je okno již maximalizované, spočtení relativní vzdálenosti kurzoru zvětšeného titlebaru
            // a aplikování vzdálenosti na normalizovaný titlebar
            if (WindowState == WindowState.Maximized)
            {
                var prevTitleBarMouse = Mouse.GetPosition(titlebar);
                double _prevousTitleBarPercentWidth = (prevTitleBarMouse.X / this.ActualWidth) * 100;


                WindowState = WindowState.Normal;

                var afterTitleBarMouse = Mouse.GetPosition(titlebar);

                this.Left = afterTitleBarMouse.X - ((this.Width / 100) * _prevousTitleBarPercentWidth);
                this.Top = afterTitleBarMouse.Y - 7;

                this.DragMove();

            }
            else
            {
                this.DragMove();
            }
        }

        /// <summary>
        /// Tlačítko pro zavření okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void titleBarClose(object sender, RoutedEventArgs e) => this.Close();

        /// <summary>
        /// Tlačítko pro zvětšení a zmenšení okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TitleBarMaximize(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        /// <summary>
        /// Tlačítko minimalizace okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void titleBarMinimize(object sender, RoutedEventArgs e) => WindowState = WindowState.Minimized;

        ///// <summary>
        ///// Při poklepání na titlebar maximalizovvat či minimalizovat okno
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void titlebar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    if(WindowState == WindowState.Maximized)
        //    {
        //        WindowState = WindowState.Normal;
        //    }else
        //    {
        //        WindowState = WindowState.Maximized;
        //    }
        //}

        /// <summary>
        /// Pokud se změní atribut state tak se změní i ikona pro max. a restore okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                titleBarMaximizeIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.WindowRestore;
            }
            else
            {
                titleBarMaximizeIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.WindowMaximize;
            }
        }
        #endregion

        private void SCConnectButon_Click(object sender, RoutedEventArgs e)
        {

        }

        private void tBoxScConnectIP_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}



