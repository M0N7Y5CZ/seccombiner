﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace analogia_projekt.Helpers
{
    class ServerStatus : IDisposable
    {
        public BackgroundWorker workerServerStatus;

        public ServerStatus()
        {
            workerServerStatus = new BackgroundWorker();
            workerServerStatus.WorkerReportsProgress = true;
            workerServerStatus.WorkerSupportsCancellation = true;
            workerServerStatus.DoWork += WorkerServerStatus_DoWork;
        }

        private void WorkerServerStatus_DoWork(object sender, DoWorkEventArgs e)
        {
            bool hostIsPingable;
            do
            {
                hostIsPingable = GetPingResponse((string)e.Argument);

                // Each time you get a response, report the result to the UI thread
                ((BackgroundWorker)sender).ReportProgress(0, hostIsPingable);
                if (workerServerStatus.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }
                Thread.Sleep(500);
            } while (hostIsPingable);
        }

        public static bool GetPingResponse(string IpAddress, int timeout = 3000)
        {
            try
            {
                var ping = new Ping();
                var reply = ping.Send(IpAddress, timeout);
                Console.WriteLine(reply.Address + " " + reply.RoundtripTime);
                return (reply.Status == IPStatus.Success);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public void Dispose()
        {
            workerServerStatus.CancelAsync();
            workerServerStatus.Dispose();
        }
    }
}
