![](/images/analogia_logo.svg)

![](/images/SecCombiner_Screenshot.PNG)

# SecCombiner
Aplikace k základnímu nastavení a diagnostice zařízení SecCombiner (dále SC) a SecCombiner Remote Controller (dále SCRC). Aplikace komunikuje se zařízeními pomocí sítě LAN skrze protokol SSH. Po připojení aplikace k zařízení je možné změnit jeho síťové nastavení a nastavit IP adresu protistrany, aby se obě zařízení spojily. Ke spojení dojde až po restartu zařízení, z čehož vyplývá i nutnost restartovat zařízení z aplikace. Restart je možný dvěma způsoby. První způsob restartuje zařízení softwarově pomocí příkazu restart. Druhá možnost je provést hardwarový restart pomocí krátkodobého odpojení napájení, o které se stará SCRC, tento úkol mu musí být sdělen pomocí příkazu. Obě zařízení mají dostupnou konzoli a VNC server, který je potřeba před připojením spustit.

## Funkce programu
1.	Zobrazení systémových logů
2.	Zobrazení HW logů
3.	Výpis USB periferií
4.  Real-Time diagnostika kamer (Žívý RTSP stream v náhledu)
